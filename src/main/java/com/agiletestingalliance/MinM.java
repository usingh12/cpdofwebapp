package com.agiletestingalliance;

public class MinMain {

    public int funct(int integer1, int integer2) {
        if (integer2 > integer1)
        {
            return integer2;
        }
        else
        {
            return integer1;
        }
    }

}
